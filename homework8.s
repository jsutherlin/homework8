	.global _start
	
_start:

@Display your first and last name

	MOV R7, #4
	MOV R0, #1
	MOV R2, #16
	LDR R1, =string1
	SWI 0
	
@Prompt user to enter between 1 to 40 characters

	MOV R7, #4
	MOV R0, #1
	MOV R2, #25
	LDR R1, =string2
	SWI 0

@Read 1 to 40 characters and save them to input string

_read:

	MOV R7, #3
	MOV R0, #0
	MOV R2, #40
	LDR R1, =input
	SWI 0

@Determine the number of characters in the input string

@Load the input string and set character counter to 0

	LDR R2, =input
	MOV R4, #0

_loop1:

@do

	LDRB R0, [R2, R4]
	CMP R0, #10
	
@if (R0 == 10)

	BEQ _next1
	
@while (R0 != 10)

	ADD R4, R4, #1
	BAL _loop1

@Create a new output string where upper case letters are saved as lower case

@Load the input string and output1 string

_next1:

	LDR R1, =input
	LDR R3, =output1
	
@Subtract 1 from the character counter

	SUB R4, R4, #1
	MOV R5, R4
	
@Reset ascii decimal and get new character

_getnew:
	
	MOV R8, #65
	MOV R9, #97
	LDRB R0, [R1, R5]
	
@Search for uppercase letters

_loop2:

@do

	CMP R0, R8
	
@if (R0 == R8)

	BEQ _lowercase
	
@if (R0 < R8)

	BLT _store
	
@if (R0 != R8)

	ADD R8, R8, #1
	CMP R8, #91
	
@while (R8 < 91)

	BLT _loop2

@if (R8 > 91)

	BAL	_loop3
	
@Uppercase letters are saved as lowercase letters

_lowercase:

	ADD R0, R0, #32
	STRB R0, [R3, R5]
	SUBS R5, R5, #1
	
@if (R5 > -1)

	BPL _getnew
	
@if (R5 == -1)

	BAL _next2
	
@Search for lowercase letters

_loop3:

@do

	CMP R0, R9
	
@if (R0 == R9)

	BEQ _uppercase
	
@if (R0 < R9)

	BLT _store
	
@if (R0 != R9)

	ADD R9, R9, #1
	CMP R9, #123
	
@while (R9 < 123)

	BLT _loop3
	
@if (R9 > 123)
	
	BAL _store
	
	
@Lower case letters are saved as upper case
	
_uppercase:

	SUB R0, R0, #32
	STRB R0, [R3, R5]
	SUBS R5, R5, #1
	
@if (R5 > -1)
	
	BPL _getnew
	
@if (R5 == -1)

	BAL _next2
	
@Stores non uppercase and non lowercase characters
	
_store:

	STRB R0, [R3, R5]
	SUBS R5, R5, #1

@if (R5 > -1)

	BPL _getnew

@Display the new output string

_next2:

	MOV R7, #4
	MOV R0, #1
	MOV R2, #41
	LDR R1, =output1
	SWI 0

@Create a new output string where the characters are reversed from the input

	LDR R1, =input
	LDR R3, =output2
	MOV R2, #0

_loop5:

@do

	LDRB R0, [R1, R4]
	STRB R0, [R3, R2]	
	ADD R2, R2, #1
	SUBS R4, R4, #1
	
@while (R4 > -1)

	BPL _loop5
	
	
@Display the new output string

	MOV R7, #4
	MOV R0, #1
	MOV R2, #41
	LDR R1, =output2
	SWI 0


_exit:

	MOV R7, #1
	SWI 0
	
.data
string1:
.ascii "James Sutherlin\n"
string2:
.ascii "Enter 1 to 40 character:\n"
input:
.ascii "                                        "
output1:
.ascii "                                        \n"
output2:
.ascii "                                        \n"
